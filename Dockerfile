FROM gradle:latest
LABEL author="Nicolas Oliveira"
COPY . /var/gradle
WORKDIR /var/gradle
ENTRYPOINT [ "gradle", "run" ]
