package com.functions

import org.jsoup.Jsoup
import org.jsoup.nodes.Document

class Scrapper(url: String) {
    var page: Document
    var url: String

    init{
        this.page = Jsoup.connect(url).userAgent("Mozilla").get()
        this.url = url
    }

    fun getTitle(): String{
        return page.select("div.col-xs-12 > h3.title").text()
    }

    fun getDescription(): String{
        return page.select("div.desc-text").text()
    }

    fun getCharpterListFromPage(number_page:String="1"): MutableMap<String, String>{
        var page_new_page = Jsoup.connect(url+"?page="+number_page).userAgent("Mozilla").get()

        val chapter_list = mutableMapOf<String, String>()

        page_new_page.select("div#list-chapter > div.row > div").forEach {
            it.select("ul > li > a").forEach {
                chapter_list.put(it.text(), it.attr("href"))
            }
        }

        return chapter_list
    }

    fun CopyChapterContent(chapter_url: String): String{
        var page_new_page = Jsoup.connect(chapter_url).userAgent("Mozilla").get()
        return page_new_page.select("div#chapter-content").text()
    }

}